var http = require('http'),
express = require('express'),
twilio = require('twilio'),
bodyParser = require('body-parser'),
MongoClient = require('mongodb').MongoClient,
sprintf = require('sprintf').sprintf,
moment = require('moment');

// Twilio Credentials
var accountSid = 'ACc3fe27debb2cdd08d6207f9d5da0728d';
var authToken = 'e8fb6915dcd75d4387e6e4300ff24bc4';

//require the Twilio module and create a REST client
// for sending messages.
var client = require('twilio')(accountSid, authToken);



// Initialize the server.
var chilidb;

// Connect to the db
MongoClient.connect("mongodb://localhost:27017/chili", function(err, db) {

  if (err) { return console.dir(err); }

  db.createCollection('actions', function(err, collection) {})

  chilidb = db.collection('actions')

});

// Start the app

var app = express();

app.use(bodyParser.urlencoded({ extended: true }));

app.set('view engine', 'ejs');

// Set the routes

app.post('/sms', function(req, res) {

  var twilio = require('twilio');
  var twiml = new twilio.TwimlResponse();

  message = req.body.Body.match(/(chili|Chili|pie|Pie|d|D)([0-9]+) ?(@?.*)/)

  //console.log("From")
  //console.log(req.body.From)
  if (message) {

    record = {
      "phone": req.body.From,
      "action": 'votes',
      "category": message[1].toLowerCase(),
      "id": message[2].toLowerCase(),
      "name": message[3].toLowerCase(),
      "comment": message[6],
      "label": message[1].toLowerCase()+message[2].toLowerCase()
    }

    //console.log(chilidb);
    console.log(record);

    twiml.message(
      sprintf("Thanks for your vote! %s %s for %s%s ", record.name, record.action, record.category, record.id)
    );

    // Now check for duplicates and change the previous to 'voted' if duplicated.
    chilidb.find({
      "phone": record.phone,
      "name": record.name,
      "action": record.action,
      "category": record.category,
    }).toArray(function (err, items) {
      if (err) {
        console.log(err);
      } else {
        if (items.length > 0) {
          console.log("A vote by this user was already done in this category. Replacing...");
          chilidb.updateMany({
            "phone": record.phone,
            "name": record.name,
            "action": record.action,
            "category": record.category,
            "_id": { "$ne": record._id }
          },
          { $set: { "action": "voted" } },
          { multi: true },
          function ( err, items ) {
            if (err) { console.log(err) }
            else {
              console.log(record._id);
              console.log("updated to voted...");
            }

            chilidb.insert(record); // after update to previous votes
          });

        } else {

          chilidb.insert(record);
        }
      }
    });


  } else {

    record = {
      "phone": req.body.From,
      "action": 'commented',
      "category": '',
      "id": '',
      "name": '',
      "comment": req.body.Body,
      "label": ''
    }

    chilidb.insert(record);
    //console.log(chilidb);
    console.log("Not a vote -- stored as a comment:")
    console.log(record);
    twiml.message('Thanks for your comment. To vote, please use the format:\n\
    dish##\
    (eg. chili12)\n\
    (add an optional @name if multiple voters from the same number)');
  }
  res.writeHead(200, {'Content-Type': 'text/xml'});
  res.end(twiml.toString());
});

app.get('/', function(req, res) {
  chilidb.find().sort( { _id: -1 }).toArray(function(err,items) {
    console.log("*******************")
    console.log(items);
    res.render('index', { "actions": items, "moment": moment, "data_hidden": false });
  });
});


app.get('/hidden', function(req, res) {
  chilidb.find().sort( { _id: -1 }).toArray(function(err,items) {
    //console.log(items);
    res.render('index', { "actions": items, "moment": moment, "data_hidden": true });
  });
});

app.get('/monitor', function(req, res) {
  chilidb.find().sort( { "phone": 1, "name": 1, "label": 1, _id: -1  }).toArray(function(err,items) {
    //console.log(items);
    res.render('monitor', { "actions": items, "moment": moment });
  });
});

http.createServer(app).listen(1340, function () {
  console.log("Express server listening on port 1340");
});
